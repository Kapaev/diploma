package diploma;

import org.apache.commons.math.distribution.NormalDistributionImpl;
import org.apache.commons.math.stat.regression.SimpleRegression;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.function.Function2D;
import org.jfree.data.function.NormalDistributionFunction2D;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.List;
import java.util.function.Function;

public class App {

    static int ROUND_ACCURACY = 3;
    static int COUNT = 3000;
    Double accuracy = Math.pow(10, -ROUND_ACCURACY);
    public static void main(String[] args) throws IOException {

        List<Double> estimations1 = new App().getEstimations();
        Double minInterval_1 = 0.0;
        Double maxInterval_1 = 0.0;
        System.out.print("Enter absolute interval: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String intervalInput = br.readLine();
        String[] intervalArray = {"6.5", "8.5"};
        if (!intervalInput.isEmpty()) {
            intervalArray = intervalInput.split(" ");
            minInterval_1 = Double.valueOf(intervalArray[0]);
            maxInterval_1 = Double.valueOf(intervalArray[1]);
        } else {
            minInterval_1 = myRound(mean(estimations1) - 3 * sd(estimations1));
            maxInterval_1 = myRound(mean(estimations1) + 3 * sd(estimations1));
        }

        Map<Double, Double> values1 = new App().getDrawDistribution(estimations1, minInterval_1, maxInterval_1);
        new App().drawGraph(values1, estimations1, minInterval_1, maxInterval_1);

        List<Double> estimations2 = new App().getEstimations();
        Double minInterval_2 = 0.0;
        Double maxInterval_2 = 0.0;
        System.out.print("Enter absolute interval: ");
        intervalInput = br.readLine();
        if (!intervalInput.isEmpty()) {
            intervalArray = intervalInput.split(" ");
            minInterval_2 = Double.valueOf(intervalArray[0]);
            maxInterval_2 = Double.valueOf(intervalArray[1]);
        } else {
            minInterval_2 = myRound(mean(estimations2) - 3 * sd(estimations2));
            maxInterval_2 = myRound(mean(estimations2) + 3 * sd(estimations2));
        }

        Map<Double, Double> values2 = new App().getDrawDistribution(estimations2, minInterval_2, maxInterval_2);
        new App().drawGraph(values2, estimations2, minInterval_2, maxInterval_2);

        Map<Interval, Double> intervalsValues_1 = new App().prepareIntervalsValues(values1, minInterval_1, maxInterval_1);
        Map<Interval, Double> intervalsValues_2 = new App().prepareIntervalsValues(values2, minInterval_2, maxInterval_2);

        List<Double> sample1 = new App().generateSample(intervalsValues_1);
        List<Double> sample2 = new App().generateSample(intervalsValues_2);

        Double[] resultSample = new App().multiplySamples(sample1, sample2);
        Arrays.sort(resultSample);

        Map<Double, Double> hystogramMap = new App().prepareValuesForGraph(resultSample);

        new App().drawGraph(hystogramMap, Arrays.asList(resultSample), resultSample[0], resultSample[resultSample.length - 1]);

        System.out.println("Mean: " + mean(estimations1)*mean(estimations2));
        double d = Math.pow(sd(estimations1)* sd(estimations2) +
                Math.pow(mean(estimations1), 2.0)*sd(estimations2) +
                Math.pow(mean(estimations2), 2.0)*sd(estimations1), 0.5);
        System.out.println("Variance: "  + d);
        System.out.println("Interval: " + (mean(estimations1)*mean(estimations2) - 3*d) +
                " " + (mean(estimations2)*mean(estimations1) + 3*d) );

    }

    private Map<Double, Double> prepareValuesForGraph(Double[] resultSample) {

        double minValue = resultSample[0];
        double maxValue = resultSample[resultSample.length - 1];
        double countOfIntervals = Math.round(Math.pow(resultSample.length, 1.0 / 3.0));
        double width = (maxValue - minValue) / countOfIntervals;

        Map<Interval, Double> hystogram = new HashMap<>();

        int i = 1;
        int count = 1;
        for (Double value: resultSample) {
            double rightBorder =  minValue + i*width;
           if (value >= rightBorder) {
               Interval interval = new Interval(minValue + (i-1)*width, minValue + i*width);
               hystogram.put(interval, (double)count/((interval.max - interval.min)*resultSample.length));
               System.out.println(count);
               count=1;
               i++;
           } else {
               count++;
           }
        }

        double sum = 0;
        Map<Double, Double> hystogramValues = new HashMap<>();
        for(Map.Entry<Interval, Double> entry: hystogram.entrySet()) {
            sum += (entry.getKey().max - entry.getKey().min) * entry.getValue();
            for(double j = entry.getKey().min; j < entry.getKey().max; j+=accuracy) {
                hystogramValues.put(j, entry.getValue());
            }
        }
        System.out.println("Sum: " + sum);
        return hystogramValues;
    }


    private Double[] multiplySamples(List<Double> sample1, List<Double> sample2) {
        List<Double> list = new ArrayList<>();

        for (Double v1: sample1) {
            for (Double v2: sample2) {
                list.add(v1*v2);
            }
        }

        return list.toArray(new Double[list.size()]);
    }


    private List<Double> generateSample (Map<Interval, Double> intervalDoubleMap) {
        List<Double> list = new ArrayList<>();

        for(Map.Entry<Interval, Double> entry: intervalDoubleMap.entrySet()) {
            Interval interval = entry.getKey();
            int valuesCount = (int) Math.round((interval.max - interval.min)*entry.getValue()* COUNT);
            for (int i = 0; i <  valuesCount; ++i) {
                Random r = new Random();
                double d = interval.min + r.nextDouble() * (interval.max - interval.min);
                list.add(d);
            }
        }

        return list;
    }

    private Map<Interval, Double> prepareIntervalsValues(Map<Double, Double> values, Double minInterval, Double maxInterval) {
        Map<Interval, Double> map = new HashMap<Interval, Double>();
        double leftBorder = minInterval;
        double rightBorder;
        double initialValue = 0.0;
        for(Map.Entry<Double, Double> entry: values.entrySet()) {
            if ((Math.abs(initialValue - entry.getValue()) > Math.pow(10.0, -6.0))) {
                rightBorder = entry.getKey();
                map.put(new Interval(leftBorder, rightBorder), initialValue);
                initialValue = entry.getValue();
                leftBorder = rightBorder;
            }
        }

        return map;
    }

    private List<Double> getEstimations() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter expert judgements: ");
        String input = br.readLine();

        String[] stringEstimations = {"5.4", "6.4", "6.5", "6.6"};
        if (!input.isEmpty()) {
            stringEstimations = input.split(" ");
        }
        List<Double> estimations = new ArrayList<Double>();
        for(String stringEstimate: stringEstimations) {
            estimations.add(Double.valueOf(stringEstimate));
        }

        return estimations;
    }

    private void drawGraph(Map<Double, Double> values, List<Double> estimations, Double minInterval, Double maxInterval ) {
        XYSeries series = new XYSeries("f(x)");

        for(Map.Entry<Double, Double> entry: values.entrySet()){
            series.add(entry.getKey(), entry.getValue());
        }

        Function2D normal = new NormalDistributionFunction2D(mean(estimations), sd(estimations));
        XYDataset dataset = DatasetUtilities.sampleFunction2D(normal, minInterval, maxInterval, 100000, "Normal");
        final JFreeChart normalChart = ChartFactory.createXYLineChart(
                "XY Series Demo",
                "x",
                "f(x)",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, true
        );

        int dataSetIndex = dataset.getSeriesCount();
         normalChart.getXYPlot().setDataset(dataSetIndex, new XYSeriesCollection(series));
        normalChart.getXYPlot().setBackgroundPaint(Color.WHITE);
        normalChart.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        normalChart.getXYPlot().setRangeGridlinePaint(Color.BLACK);


        JFrame frame =
                new JFrame("MinimalStaticChart");
        frame.getContentPane()
                .add(new ChartPanel(normalChart));
        frame.setSize(800, 600);
        frame.show();

    }
    private Map<Double, Double> getDrawDistribution(List<Double> estimations, double minInterval, double maxInterval) {

        Collections.sort(estimations);
        double N = estimations.size();
        double mu = myRound(1 / N); // 1/N
        double alpha = myRound(1 / (N + 1));
        double rho = myRound((maxInterval - minInterval) / (N + 2));
        if (rho + estimations.get(estimations.size()- 1)> maxInterval ) {
            rho = maxInterval - estimations.get(estimations.size()- 1);
        }
        if (estimations.get(0) - rho < minInterval) {
            rho = estimations.get(0) - minInterval;
        }
        System.out.println("Rho: " + rho);
        System.out.println("Mean: " + mean(estimations));
        System.out.println("Std: " + sd(estimations));
        System.out.println("Left: " + (mean(estimations) - 3*sd(estimations)));
        System.out.println("Right: " + (mean(estimations) + 3*sd(estimations)));


        double apriori = 1 / (maxInterval - minInterval);
        double amplitude = 1 / (2 * rho);

        Map<Interval, Double> psis = new HashMap<Interval, Double>();

        for (Double estimation: estimations) {
            Interval interval = new Interval();
            interval.min = myRound(estimation - rho);
            interval.max = myRound(estimation + rho);
            double value = myRound((1 - alpha) * amplitude * mu);

            psis.put(interval, value);
        }

        psis.put(new Interval(myRound(minInterval), myRound(maxInterval)), apriori * alpha); //apriory psi
        //psis.put(new Interval(myRound(minInterval-100*accuracy), myRound(maxInterval + 100 * accuracy)), 0.0); //empty psi

        Map<Double, Double> seriesMap = new TreeMap<Double, Double>();

        double sum = 0;
        for (Interval interval: psis.keySet()) {

            sum += (interval.max - interval.min) * psis.get(interval);

            for(double i = interval.min; i < interval.max; i+=accuracy){
                Double y = seriesMap.get(myRound(i));
                if (y != null) {
                    seriesMap.put(myRound(i), myRound(y + psis.get(interval)));
                } else {
                    seriesMap.put(myRound(i), myRound(psis.get(interval)));
                }
            }
        }

        System.out.println(seriesMap);
        System.out.println(sum);
        return seriesMap;
    }

    private static double myRound(double doubleValue) {
        return new BigDecimal(doubleValue).setScale(ROUND_ACCURACY, RoundingMode.HALF_UP).doubleValue();
    }

    private static double mean(List<Double> sample) {
        double sum = 0;
        for (Double value: sample) {
            sum += value;
        }

        return sum / ((double) sample.size());
    }

    private static double sd(List<Double> sample) {
        double sum = 0;
        double mean = mean(sample);
        for (Double value: sample) {
            sum += Math.pow(value - mean, 2);
        }

        return Math.sqrt(sum / ((double) sample.size() - 1.0));
    }

    private class Interval {

        double min;
        double max;

        public Interval() {
        }

        public Interval(double min, double max) {
            this.min = min;
            this.max = max;
        }
    }
}